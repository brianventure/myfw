<?php

class Auth extends Eloquent
{
	protected $table = 'users';
	protected $primaryKey = 'id';
	public $timestamps = true;

	public static function attempt($data, $remember = false)
	{
		$Auth = Auth::where('password','=',getHash($data['password']));

		foreach ($data as $key => $value) {
			if($key != 'password')
				$Auth->where($key,'=',$value);
		}

		$User = $Auth->get()[0];
		if(count($User) > 0)
		{
			$User->login_token = md5(time());
			Session::put('loginid', $User->loginid);
			Session::put('login_token', $User->login_token);
			$user_groups_id = UserGroups::getIdByCode($User->user_groups_code);
			$User->permissions = Permissions::findByGroupID($user_groups_id);

			Cache::put($User->loginid,$User,60*60);
			return $User;
		}
		return False;
	}

	public static function check()
	{
		if(Session::has('loginid') && 
			Cache::has(Session::get('loginid')) && 
			Session::get('login_token') == Cache::get(Session::get('loginid'))->login_token)
		{
			Cache::put(Session::get('loginid'), Cache::get(Session::get('loginid')),60*60);
			return True;
		}
		static::logout();
		return False;
	}

	public static function logout()
	{
		if(Session::has('loginid'))
			Cache::forget(Session::get('loginid'));
		return Session::flush();
	}

	public static function user()
	{
		if(Session::has('loginid') && Cache::has(Session::get('loginid')))
		{
			return Cache::get(Session::get('loginid'));
		}
		return False;
	}

	public static function loginUsingId($id)
	{
		$User = static::find($id);
		if(!is_null($User))
		{
			$User->login_token = md5(time());
			Session::put('loginid', $User->loginid);
			Session::put('login_token', $User->login_token);
			$user_groups_id = UserGroups::getIdByCode($User->user_groups_code);
			$User->permissions = Permissions::findByGroupID($user_groups_id);

			Cache::put($User->loginid,$User,60*60);
			return $User;
		}
		return false;
	}

	public static function switchTo($id)
	{
		if(Session::get('original_users_id') == '')
		{
			Session::put('original_users_id',Auth::user()->id);
			Session::put('original_users_loginid',Auth::user()->loginid);
			$original_loginid = Auth::user()->loginid;
		}else{
			$original_loginid = Session::get('original_users_loginid');
		}

		self::loginUsingId($id);
		$new_loginid = Auth::user()->loginid;
		Audit::action('Switching User Accessibility')->content($original_loginid . ' has switch to ' . $new_loginid)->status(1)->log();
	}

	public static function switchBack()
	{
		$id = Session::get('original_users_id');
		$original_loginid = Auth::user()->loginid;
		self::loginUsingId($id);
		$new_loginid = Auth::user()->loginid;
		Session::forget('original_users_id');
		Session::forget('original_users_loginid');
		Audit::action('Switching User Accessibility')->content($original_loginid . ' has switch back to ' . $new_loginid)->status(1)->log();
	}
}