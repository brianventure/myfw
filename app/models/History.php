<?php

class History extends Eloquent
{
	protected $table = 'table_versions';
	protected $primaryKey = 'id';
	public $timestamps = true;
	public $toucher = true;


	public static function findByTableName()
	{
		if(func_num_args() > 0)
		{
			$wildCardField = func_get_args();
			$obj = static::where('table_name','=',$wildCardField[0]);
			for ($i=1; $i < func_num_args(); $i++) { 
				$obj->where('original_data','LIKE','%'.addslashes($wildCardField[$i]).'%');
			}
			return $obj->orderBy('updated_at','desc');
		}
	}
}