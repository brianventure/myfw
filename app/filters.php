<?php
Route::filter('auth',function(){
	if(!Auth::check())
	{
		return Redirect::route('login')->with('error','Please login');
	}
});

Route::filter('active_account',function(){

});

Route::filter('csrf',function(){
	if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
		return;
	
	if(!Session::has('_token') || Session::get('_token') != Input::get('_token'))
	{
		(new LoginRepository)->logout();
		return Redirect::route('login')->with('error','Invalid Request. Please try again.');
		Session::forget('_token');
	}
	Session::forget('_token');
});
