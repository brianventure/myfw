<?php
return [
	'default' => Config::get('environment.'.(getthehostname()).'.database_connection'),
	'connections' => [
		'mysql' => array(
			'driver'    => 'mysql',
			'host'      => '127.0.0.1',
			'database'  => 'test',
			'username'  => 'root',
			'password'  => '',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'port' 		=> '',
			'prefix'    => '',
			'options'	=> [
				PDO::ATTR_PERSISTENT => true,
			],
		),
	],
];