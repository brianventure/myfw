<?php

return [
	// apc,memcached,file
	'default' => 'apc',

	'memcached' => array(

		array('host' => '127.0.0.1', 'port' => 11211, 'weight' => 100),

	),

];