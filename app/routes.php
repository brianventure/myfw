<?php
include_once __dir__ . '/routes/binds.php';

foreach (glob(__dir__."/routes/*.php") as $filename) {
	if($filename != 'binds.php')
	    include_once $filename;
}

Route::get('test',['as'=>'testing','do'=>function(){
	// Audit::content('test')->action('testing')->log();
	var_dump(Route::getCurrentPath());
	var_dump(Route::getCurrentRoute());
	var_dump(Route::routesName());
	return 'done';
}]);