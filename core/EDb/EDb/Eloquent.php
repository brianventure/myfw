<?php
 
class Eloquent extends \EDb\EloquentBuilder
{
	use \EDb\EloquentMagic;
    
	protected static $DbTable = null;
	protected static $forUpdateDbTable = null;

	protected $table = '';
	protected $connection = NULL;
	protected $primaryKey = 'id';
	private $called_class = null;
	public $timestamps = true;
	public $toucher = false;
	protected $modelClass;

	function __construct() {
		static::$DbTable = null;
	}

	public function destruction()
	{
		static::$DbTable = null;
		static::$forUpdateDbTable = null;
	}

	function boot() 
	{
		$this->called_class = get_called_class();
		$this->table = empty($this->table)? strtolower($this->called_class.'s'):$this->table;
		static::$DbTable = DB::connect($this->connection)->table($this->table,$this->called_class);
	}

	protected static function bootup()
	{		
		$class = get_called_class();
		$self = new $class;
		$self->boot();
		return $self;
	}

	protected function hasOne($modelClass, $foreignKey = '')
	{
		return [$modelClass => $foreignKey];
	}

	protected function belongsTo($modelClass, $foreignKey = '')
	{
		return [$modelClass => $foreignKey];
	}
}