<?php namespace EDb;

class EloquentBuilder
{
	
	public static function with($modelClass)
	{
		$self = static::bootup();

		$joinMethod = strtolower($modelClass);
		$join1 = $self->$joinMethod();
		$foreignKey1 = $join1[$modelClass];
		$self->joinModel = $modelClass;

		$joinClass = (new $modelClass);
		$joinClass->boot();
		$joinMethod = strtolower($self->called_class);
		$join2 = $joinClass->$joinMethod();
		$foreignKey2 = $join1[$modelClass];

		return DB::table($self->table,$self->called_class)->joinBind([
				$modelClass => [
					'select' => '*',
					'table'	=> $joinClass->table,
					'foreignKey' => $foreignKey1,
					'joinKey'	=> $foreignKey2,
				]
			]);
	}

	public static function where($where, $operator = '=', $what = '')
    {
    	$self = static::bootup();
		return DB::table($self->table,$self->called_class)->where($where, $operator, $what);
    }

    public static function skip($skip)
    {
    	$self = static::bootup();
		return DB::table($self->table,$self->called_class)->skip($skip);
    }

    public static function take($take)
    {
    	$self = static::bootup();
		return DB::table($self->table,$self->called_class)->take($take);
    }

	public static function all()
	{
		$self = static::bootup();
		$results = DB::table($self->table,$self->called_class)->get();
		$result_objects = [];
		if(isset($results))
		{
			foreach ($results as $result) {
				$result_objects[] = Cast::toObject($self->called_class,$result);
			}
		}

		return $result_objects;
	}

	public function delete()
	{
		if(is_null(static::$forUpdateDbTable))
		{
			$self = static::bootup();
			return DB::table($self->table,$self->called_class)->delete();
		}
		else
		{
			return static::$forUpdateDbTable->delete();
		}
	}

	public static function find($id)
	{
		$self = static::bootup();
		static::$forUpdateDbTable = static::$DbTable;
		$DbTable = static::$forUpdateDbTable->where($self->primaryKey,'=',$id);
		$result = $DbTable->take(1)->get();
		return isset($result[0])? Cast::toObject($self->called_class,$result[0]):Null;
	}

	public function save($debug = false)
	{
		$options = [];
		$blacklisted = ['primaryKey','table','called_class','timestamps','modelClass','toucher','connection'];
		foreach ($this as $key => $value) {
			if ( !in_array($key,$blacklisted) ) {
				$options[$key] = $value;
			}
		}
		if(is_null(static::$forUpdateDbTable) || 
			!isset($options[$this->primaryKey]) || 
			$options[$this->primaryKey] == '')
		{
			return static::create($options,$debug);
		} else {
			if($this->timestamps)
			{
				$options['updated_at'] = date('Y-m-d H:i:s');
			}
			if($this->toucher)
			{
				$options['updated_by'] = \Auth::check()? (Session::has('original_users_loginid')? Session::get('original_users_loginid'):\Auth::user()->loginid):'System';
			}
			return static::update($options,$debug);
		}
	}

	public static function create(array $options = [],$debug = false)
	{
		$self = static::bootup();	

		if($self->timestamps)
		{
			$options['updated_at'] = date('Y-m-d H:i:s');
			$options['created_at'] = date('Y-m-d H:i:s');
		}
		if($self->toucher)
		{
			$options['updated_by'] = \Auth::check()? (Session::has('original_users_loginid')? Session::get('original_users_loginid'):\Auth::user()->loginid):'System';
			$options['created_by'] = \Auth::check()? (Session::has('original_users_loginid')? Session::get('original_users_loginid'):\Auth::user()->loginid):'System';
		}
		$DB = static::$DbTable;
		return ($debug? $DB->debug()->insert($options):$DB->insert($options));
	}

	public static function update(array $options = [],$debug = false)
	{
		$DB = static::$forUpdateDbTable;
		return ($debug? $DB->debug()->update($options):$DB->update($options));
	}
}