<?php namespace Biew;

class Biew
{
    protected $baseViewDir = 'views';
    protected $data = [];
    protected $view_file;

    function __construct() {
        $this->baseViewDir = __dir__.'/../../app/views';
    }

    public function __toString()
    {
        return $this->render();
    }

    public function render()
    {
        extract( $this->data );
        ob_start();
        is_file( $this->view_file ) ? require $this->view_file : False;
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
    }

    public function with($variable, $value='')
    {
        if(!is_array($variable))
        {
            $this->data = array_merge($this->data,[$variable=>$value]);
        }else{
            $this->data = array_merge($this->data,$variable);
        }

        return $this;
    }

    public function make( $view , $data = [] )
    {
        $this->data = array_merge($this->data,$data);
        $view_file = $this->baseViewDir .'/'. (str_replace('.','/',$view)) . '.php';
        $this->view_file = is_file( $view_file )? 
            $view_file :
            $this->baseViewDir .'/errors/404.php';
        
        return $this;
    }
}