<?php namespace DiLioc;

class DiLioc
{
	private $_containers = [];

	public function has($key)
	{
		return isset($this->_containers[$key]);
	}

	public function bind($key, $map)
	{
		$args = $this->getArgs(func_get_args());
		$this->_containers[$key] = [$key, $map, $args];
	}

	public function make($key)
	{
		list($key, $map, $args) = $this->_containers[$key];

		return $this->callback($key, $map, $args);

	}

	protected function callback($key, $map, $args)
	{
		if(is_string($map))
		{
			$class  = new \ReflectionClass($map);
	
			if(count($args) > 0)
			{
				return $class->newInstanceArgs($args);		
			}else{
				return $this->callbackWithNoArgs($class);
			}
		}elseif(is_callable($map)){
			return call_user_func_array($map,$args);
		}else{
			return $map;
		}
	}

	protected function getArgs($arg_list,$num_for_shift = 2)
	{
		$i = 0;
		while ($i < $num_for_shift) {
			array_shift($arg_list);
			$i++;
		}

		return $arg_list;
	}

	protected function callbackWithNoArgs($class)
	{
		$constructor = $class->getConstructor();
		if(count($constructor) > 0)
		{
			$parameters = $constructor->getParameters();
			$DIs=[];
			foreach ($parameters as $param) {
				$di = $param->getClass()->name;
				if(!isset($this->_containers[$di]))
				{
					$this->bind($di,function() use($di) {
						return new $di;
					});
				}
				$DIs[]=$this->make($di);
			}
			
			return $class->newInstanceArgs($DIs);
		}else{
			return $class->newInstance();
		}
	}
}