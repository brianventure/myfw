<?php namespace DiLioc;

class App
{
	use \McCade\McCade;

	public function __construct()
	{
		$App = new DiLioc;
		$this->load($App);
	}
}