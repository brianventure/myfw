<?php namespace McCade;

/**
 * Do not use this when McCade used
 */

trait McSetterGetter
{
	public function __call($name,$args)
	{
		if(preg_match('/^set/i', $name))
		{
			$property = preg_replace('/^set(.+)/i', '$1', strtolower($name));
			$this->{$property} = $args[0];
		}

		if(preg_match('/^get/i', $name))
		{
			$property = preg_replace('/^get(.+)/i', '$1', strtolower($name));
			return $this->{$property};
		}	
	}
}