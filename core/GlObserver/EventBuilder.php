<?php namespace GlObserver;

class EventBuilder 
{
	use GlObserver;	

	public function listen($Observer, $map = '')
	{
		$this->__attach($Observer, $map);
	}

	public function fire($Observer = null, $args = [])
	{
		$this->__notify($Observer,$args);
	}

	public function kill($Observer = null)
	{
		$this->__detach($Observer);
	}
}