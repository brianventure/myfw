<?php

function route($route, $args = [])
{
    return Route::route($route, $args);
}

function assets($stuff='')
{
	return (new Route)->Url .'/'.$stuff;
}

function include_layout($file='')
{
	include_once(__dir__.'/../../app/views/'.$file);
}

function getHash( $str )
{
    if ( empty( $str ) )
    {
        throw new \Exception( 'Could not hash with empty string.' );
    }

    $salt_key = '-"[KWnDPHqWqW+sfa!T"3;qiK?du+5XMm+u:Jcdas >~MOM:kFL#1$-*Gd|7VD/1B8&ADB* 12{-"T"3;qiK?du+5XMm';
    if ( strlen( $salt_key ) > 0 )
    {
        return md5( " " . md5( $str ) . $salt_key );
    }
}

function dd($info)
{
	die(var_dump($info));
}

function uuid() {
    return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
        // 32 bits for "time_low"
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
        // 16 bits for "time_mid"
        mt_rand( 0, 0xffff ),
        // 16 bits for "time_hi_and_version",
        // four most significant bits holds version number 4
        mt_rand( 0, 0x0fff ) | 0x4000,
        // 16 bits, 8 bits for "clk_seq_hi_res",
        // 8 bits for "clk_seq_low",
        // two most significant bits holds zero and one for variant DCE1.1
        mt_rand( 0, 0x3fff ) | 0x8000,
        // 48 bits for "node"
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
    );
}

function cleanInput($input) {
 
  $search = array(
    '@<script[^>]*?>.*?</script>@si',   // Strip out javascript
    '@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
    '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
    '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments
  );
 
    $output = preg_replace($search, '', $input);
    return $output;
  }

function sanitize($input) {
    if (is_array($input)) {
        foreach($input as $var=>$val) {
            $output[$var] = sanitize($val);
        }
    }
    else {
        if (get_magic_quotes_gpc()) {
            $input = stripslashes($input);
        }
        $input  = cleanInput($input);
        $output = mysql_real_escape_string($input);
    }
    return $output;
}