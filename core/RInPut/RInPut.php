<?php namespace RInPut;

class RInPut
{
	protected function getAll()
	{
		return array_merge($_REQUEST,$_FILES);
	}

	public function get($key, $default = '')
	{
		if(strpos($key, '.'))
		{
			return $this->getArr($key);
		}

		return $this->has($key)? $this->getAll()[$key]:$default;
	}

	public function has($key)
	{
		return isset($this->getAll()[$key]);
	}

	public function all()
	{
		return $this->getAll();
	}

	protected function getArr($key)
	{
		$temp = $this->getAll();
		$keys = explode('.', $key);
		foreach ($keys as $key) {
			if(isset($temp[$key]))
			{
				$temp = $temp[$key];
			}
		}
		return $temp;
	}

	public function only($args)
	{
		$wanted = func_get_args();
		$gets = [];
		foreach ($wanted as $key) {
			if($this->has($key))
				$gets[$key] = $this->get($key);
		}
		return $gets;
	}

	public function except($args)
	{
		$unwanted = func_get_args();
		$all = $this->all();
		foreach ($unwanted as $key) {
			unset($all[$key]);
		}
		return $all;
	}

	public function flash()
	{
		Session::put($this->all());
	}

	public function flashOnly($args)
	{
		$gets = call_user_func_array([$this,'only'], func_get_args());
		Session::put($gets);
	}

	public function flashExcept($args)
	{
		$gets = call_user_func_array([$this,'except'], func_get_args());
		Session::put($gets);
	}

	public function old($key, $default = '')
	{
		return Session::get($key, $default);
	}

	public function file($key)
	{
		if(is_array($_FILES[$key]['name']))
		{
			$files = [];
			$file_array = $this->reArrayFiles($_FILES[$key]);
				foreach ($file_array as $file) {
					$files[] = new Uploader($file);
				}
			return $files;
		}
		
		return new Uploader($_FILES[$key]);
	}

	public function hasFile($key)
	{
		if(!isset($_FILES[$key]))
			return False;
		return is_uploaded_file($_FILES[$key]['tmp_name']);
	}

	protected function reArrayFiles($file_post) {

	    foreach( $file_post as $key => $all ){
	        foreach( $all as $i => $val ){
	            $new[$i][$key] = $val;    
	        }    
	    }
	    return $new;
	}
}