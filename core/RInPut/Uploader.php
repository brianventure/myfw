<?php namespace RInPut;

use \SplFileInfo;

class Uploader
{
    protected $file;
    protected $info;

    function __construct($file) {
        $this->file = $file;
    }

    public function move($destinationPath, $filename = '')
    {
        $filename = empty($filename)? $this->file['name']:$filename;
        $this->realPath = "$destinationPath/$filename";
        if (move_uploaded_file($this->file['tmp_name'], $this->realPath)) {
            return True;
        }
        return False;
    }

    public function getRealPath()
    {
        return (new SplFileInfo($this->realPath))->getPath();
    }

    public function getClientOriginalName()
    {
        return (new SplFileInfo($this->file['name']))->getBasename();
    }

    public function getClientOriginalExtension()
    {
        return (new SplFileInfo($this->file['name']))->getExtension();
    }

    public function getSize()
    {
        return (new SplFileInfo($this->file['tmp_name']))->getSize();
    }

    public function getMimeType()
    {
        return $this->file['type'];
    }

}