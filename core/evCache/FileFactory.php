<?php namespace evCache;

class FileFactory implements CacheFactoryInterface
{
    protected $storages;

    function __construct() {
        $this->storages = __dir__ . '/../../app/storages';
    }

	public function isCached($key){
        $path = $this->getPath($key);
        $exists=file_exists($path);
        return $exists;
    }

    public function getCache($key){
        $path = $this->getPath($key);
        if (file_exists($path)) {
            $contents = file_get_contents($path);
            if ($contents) {
                $unserialised = unserialize($contents);
                if ($unserialised) {
                    return $unserialised;
                }
            }
        }
    }

    public function setCache($key, $value, $ttl = 0)
    {
        $path = $this->getPath($key);
        return file_put_contents($path, serialize($value));
    }

    public function getPath($key)
    {
        return $this->getKey($key);
    }

    public function getKey($key)
    {
        return $this->storages . '/' . $key;
    }

    public function clearCache($key = '')
    {
        $path = $this->getPath($key);
        return unlink($path);
    }
}