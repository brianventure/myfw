<?php namespace evCache;

use \Memcached;

class MemcachedFactory implements CacheFactoryInterface
{
	protected $mc;

	function __construct() {
		$this->mc = new Memcached(); 
		$this->mc->addServer(Config::get('cache.memcached.host'), 
			Config::get('cache.memcached.port'),
			Config::get('cache.memcached.weight')); 
	}

	public function isCached($key){
        return $this->mc->get($key);
    }

    public function getCache($key){
        $result =  $this->mc->get($key);
        if($result){
            return $result;
        }
        return false;
    }

    public function setCache($key, $value, $ttl = 0){
        return $this->mc->set($key, $value, $ttl);
    }

    public function clearCache($key = '')
    {
    	return $this->mc->delete($key);
    }
}