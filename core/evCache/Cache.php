<?php namespace evCache;

class Cache
{
	use \McCade\McCade;

	public function __construct()
	{
		$Cache = new evCacheBuilder();
		$this->load($Cache);
	}
}