<?php namespace evCache;

class ApcFactory implements CacheFactoryInterface
{
    public function isCached($key){
        return apc_exists($key);
    }

    public function getCache($key){
        if(apc_exists($key)){
            $result =  apc_fetch($key);
            if($result){
                return $result;
            }
        }
        return false;
    }

    public function setCache($key, $value, $ttl = 0){
        return apc_store($key, $value, $ttl);
    }

    public function clearCache($key = '')
    {
    	return apc_clear_cache($key);
    }
}