<?php namespace Paggy;

class Paggy
{
	public $items = null;
	public $links = [];
	public $totalItems = 0;
	public $perPage = 0;
	public $pages = 0;
	public $currentPage = 1;
	protected static $currentUrl = '';

	public function make($items, $totalItems, $perPage)
	{
		static::$currentUrl = "http://$_SERVER[HTTP_HOST]/" . trim(trim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH),"/"),"?") . '/?';
		$this->currentPage = isset($_GET['page']) ? ((int) $_GET['page']) : $this->currentPage;
		$this->totalItems = $totalItems;
		$this->perPage = $perPage;
		$this->pages = round($totalItems/$perPage); 
		$this->maxSlider = 5;
		$this->render();
		return $this;
	}

	public function links()
	{
		return implode('',$this->links);
	}

	public function render() 
	{ 
      
      if ($this->pages != 1) { 
        $previous_page = $this->currentPage-1; 
        $next_page     = $this->currentPage+1; 

        if ($previous_page < 1) { $previous_page=1; } 
        if ($next_page > $this->pages) { $next_page=$this->pages; } 

        $this->links[] = '<ul class="pagination">'; 

        if ($this->currentPage != 1) { 
          $this->links[] = '<li><a href="'.static::$currentUrl.'&page=1">'.'&lt;&lt;'.'</a></li>'; 
          $this->links[] = '<li><a href="'.static::$currentUrl.'&page='.$previous_page.'">'.'&lt'.'</a></li>'; 
        } 
       
        for ($i=1; $i<=$this->pages; $i++) { 
          if ($this->currentPage == $i) { 
            $this->links[] = '<li class="current">'.$i.'</li>'; 
          } 
          else { 
            $this->links[] = '<li><a href="'.static::$currentUrl.'&page='.$i.'">'.$i.'</a></li>';     
          } 
        }  

        if ($this->currentPage != $this->pages) { 
          $this->links[] = '<li><a href="'.static::$currentUrl.'&page='.$next_page.'">'.'&gt;'.'</a></li>'; 
          $this->links[] = '<li><a href="'.static::$currentUrl.'&page='.$this->pages.'">'.'&gt;&gt;'.'</a></li>'; 
        } 

        $this->links[] = '</ul>'; 

      } 

    } 

}