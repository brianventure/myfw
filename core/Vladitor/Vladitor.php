<?php namespace Vladitor;

class Vladitor extends VladitorMessages
{
    protected $_errors = [];

    public function make( $data , $rules , $messages = [] )
    {
        $i =0;
    	$this->_errors = [];
        foreach ( $rules as $fieldName => $rule )
        {
            $each_rules = preg_match( '/\|+/' , $rule ) ?
                explode( '|' , $rule ) : [$rule];

            foreach ( $each_rules as $callback_rule )
            {
                $expectedValues = [];

                if ( preg_match( '/:+/' , $callback_rule ) )
                {
                    $arr = explode( ':' , $callback_rule );
                    $callback_rule = $arr[0];
                    unset( $arr[0] );
                    $expectedValues = array_values( $arr );
                }

                if ( preg_match( '/^confirmed$/' , $callback_rule ) )
                {
                    $this->{$callback_rule}(
                        @$data[$fieldName] ,
                        @$data[$fieldName . '_confirmation'] ,
                        $this->getDefaultMessage(
                            $callback_rule , $fieldName , $messages ,
                            $expectedValues
                        )
                    );
                } elseif ( preg_match( '/^same/' , $callback_rule ) )
                {
                    $this->{$callback_rule}(
                        @$data[$fieldName] , $data ,
                        $this->getDefaultMessage(
                            $callback_rule , $fieldName , $messages ,
                            $expectedValues
                        ) , $expectedValues
                    );
                } else
                {
                    $this->{$callback_rule}(
                        @$data[$fieldName] ,
                        $this->getDefaultMessage(
                            $callback_rule , $fieldName , $messages ,
                            $expectedValues
                        ) , $expectedValues
                    );
                }
            }
        }

        return $this;
    }

    public function fails()
    {
        if ( count( $this->_errors ) > 0 )
        {
            return TRUE;
        }
        return FALSE;
    }

    public function setErrors($error)
    {
        $this->_errors[] = $error;
    }

    public function errors()
    {
        return $this->_errors;
    }

    protected function required( $value ='', $message ='' )
    {
        if(is_array($value))
        {   
            if(isset($value['name']))
                $value = $value['name'];
            else
                $value = 'pass';
        }
        if ( ! isset( $value ) OR strlen( $value ) == 0 OR is_null($value))
        {
            array_push( $this->_errors , $message );
        }    

    }

    protected function mimes(array $value, $message , $expectedValue)
    {
        if($value['name'] == '')
            return;
        
        list($app,$type) = explode('/',$value['type']); 
        $types = explode(',', $expectedValue[0]);

        if ( !in_array($type,$types) )
        {
            array_push( $this->_errors , $message );
        } 
    }

    protected function size($value , $message , $expectedValue)
    {
        if(is_array($value))
        {
            if(empty($value['name']))
                return;
            $value = $value['size'];
        }
        if ( $value > $expectedValue[0] )
        {
            array_push( $this->_errors , $message );
        } 
    }

    protected function unique( $value , $message , $expectedValue )
    {
        if ( empty( $value ) ) return;

        list($table , $field) = explode( '.' , $expectedValue[0] );

        $result = DB::table($table)->select( 'count(1) as exist' )->where( $field ,'=',
                $value )->get();

        if ( $result[0]->exist > 0 )
        {
            array_push( $this->_errors , $message );
        }
    }

    protected function minlen( $value , $message , $expectedValue )
    {
        if ( empty( $value ) ) return;

        if ( strlen( $value ) < $expectedValue[0] )
        {
            array_push( $this->_errors , $message );
        }
    }

    protected function maxlen( $value , $message , $expectedValue )
    {
        if ( empty( $value ) ) return;

        if ( strlen( $value ) > $expectedValue[0] )
        {
            array_push( $this->_errors , $message );
        }
    }

    protected function betweenlen( $value , $message , $expectedValue )
    {
        if ( empty( $value ) ) return;

        list($from , $to) = explode( ',' , $expectedValue[0] );
        if ( strlen( $value ) < $from OR strlen( $value ) > $to )
        {
            array_push( $this->_errors , $message );
        }
    }

    protected function equallen( $value , $message , $expectedValue )
    {
        if ( empty( $value ) ) return;

        if ( strlen( $value ) != $expectedValue[0] )
        {
            array_push( $this->_errors , $message );
        }
    }

    protected function regexp( $value , $message , $expectedValue )
    {
        if ( empty( $value ) ) return;

        if ( !preg_match( '/' . $expectedValue[0] . '/' , $value ) )
        {
            array_push( $this->_errors , $message );
        }
    }

    protected function email( $value , $message )
    {
        if ( empty( $value ) ) return;

        if ( !filter_var( $value , FILTER_VALIDATE_EMAIL ) )
        {
            array_push( $this->_errors , $message );
        }
    }

    protected function url( $value , $message )
    {
        if ( empty( $value ) ) return;

        if ( !filter_var( $value , FILTER_VALIDATE_URL ) )
        {
            array_push( $this->_errors , $message );
        }
    }

    protected function bool( $value , $message )
    {
        if ( empty( $value ) ) return;

        if ( !filter_var( $value , FILTER_VALIDATE_BOOLEAN ) )
        {
            array_push( $this->_errors , $message );
        }
    }

    protected function number( $value , $message )
    {
        if ( empty( $value ) ) return;

        if ( ! is_numeric( $value ) )
        {
            array_push( $this->_errors , $message );
        }
    }

    protected function confirmed( $value , $value_confirmation , $message )
    {
        if ( empty( $value ) ) return;

        if ( !preg_match( '/^' . $value . '$/' , $value_confirmation ) )
        {
            array_push( $this->_errors , $message );
        }
    }

    protected function same( $value , $data , $message , $expectedValue )
    {
        if ( empty( $value ) ) return;

        if ( !preg_match( '/^' . $value . '$/' , $data[$expectedValue[0]] ) )
        {
            array_push( $this->_errors , $message );
        }
    }

    protected function between( $value , $message , $expectedValue )
    {
        if ( empty( $value ) ) return;

        list($from , $to) = explode( ',' , $expectedValue[0] );
        if ( $value < $from OR $value > $to )
        {
            array_push( $this->_errors , $message );
        }
    }

    protected function min( $value , $message , $expectedValue )
    {
        if ( empty( $value ) ) return;

        if ( $value <= $expectedValue[0] )
        {
            array_push( $this->_errors , $message );
        }
    }

    protected function max( $value , $message , $expectedValue )
    {
        if ( empty( $value ) ) return;

        if ( $value >= $expectedValue[0] )
        {
            array_push( $this->_errors , $message );
        }
    }

    protected function equal( $value , $message , $expectedValue )
    {
        if ( empty( $value ) ) return;

        if ( $value != $expectedValue[0] )
        {
            array_push( $this->_errors , $message );
        }
    }

    protected function date( $value , $message )
    {
        if ( empty( $value ) ) return;

        $format = 'Y-m-d';

        if ( date( $format , strtotime( $value ) ) != $value )
        {
            array_push( $this->_errors , $message );
        }
    }

    private function calculateYear($date)
    {
        $birth = new \DateTime($date);
        $today = new \DateTime();
        $diff = $birth->diff($today);
        return $diff->format('%y');
    }

    protected function ageElderThan( $value , $message , $expectedValue )
    {
        if ( empty( $value ) ) return;

        $format = 'Y-m-d';

        $date = date( $format , strtotime( $value ) );

        if ( $date != $value )
        {
            array_push( $this->_errors , 'Invalid date format.' );
        }
        elseif ( $this->calculateYear($date) <= $expectedValue[0] )
        {
            array_push( $this->_errors , $message );
        }
    }

    protected function ageYoungerThan( $value , $message , $expectedValue )
    {
        if ( empty( $value ) ) return;

        $format = 'Y-m-d';

        $date = date( $format , strtotime( $value ) );

        if ( $date != $value )
        {
            array_push( $this->_errors , 'Invalid date format.' );
        }
        elseif ( $this->calculateYear($date) > $expectedValue[0] )
        {
            array_push( $this->_errors , $message );
        }
    }

    protected function time( $value , $message )
    {
        if ( empty( $value ) ) return;

        $format = 'H:i:s';
        if ( date( $format , strtotime( $value ) ) != $value )
        {
            array_push( $this->_errors , $message );
        }
    }

    protected function datetime( $value , $message )
    {
        if ( empty( $value ) ) return;

        $format = 'Y-m-d H:i:s';
        if ( date( $format , strtotime( $value ) ) != $value )
        {
            array_push( $this->_errors , $message );
        }
    }

    protected function captcha( $value , $message )
    {
        if ( empty( $value ) ) return;

        if ( Session::get('captcha_answer') != $value )
        {
            array_push( $this->_errors , $message );
        }
    }

}