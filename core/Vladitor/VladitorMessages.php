<?php namespace Vladitor;

abstract class VladitorMessages
{
	protected function getDefaultMessage( $rule , $fieldName ,
        $userMessages = [] , $expectedValue = [] )
    {
        if ( isset( $userMessages[$fieldName][$rule] ) )
                return $userMessages[$fieldName][$rule];

        $fieldName = ucfirst( str_replace( '_' , ' ' , $fieldName ) );

        switch ( $rule )
        {
            case 'required':
                $msg = $fieldName . ' is required.';
                break;
            case 'bool':
                $msg = $fieldName . ' is not boolean.';
                break;
            case 'email':
            case 'url':
                $msg = $fieldName . ' is not a valid ' . $rule . '.';
                break;
            case 'number':
                $msg = $fieldName . ' is not a valid ' . $rule . '.';
                break;
            case 'size':
                $msg = $fieldName . ' is should not over ' . $expectedValue[0] . ' bytes.';
                break;
            case 'mimes':
                $msg = $fieldName . ' must be ' . $expectedValue[0] . '.';
                break;
            case 'min':
                $msg = $fieldName . ' is should be greater than ' . $expectedValue[0] . '.';
                break;
            case 'max':
                $msg = $fieldName . ' is should be lesser than ' . $expectedValue[0] . '.';
                break;
            case 'ageElderThan':
                $msg = $fieldName . ' should be elder than ' . $expectedValue[0] . ' years old.';
                break;
            case 'ageYoungerThan':
                $msg = $fieldName . ' should be younger than ' . $expectedValue[0] . ' years old.';
                break;
            case 'minlen':
                $msg = 'Length of ' . $fieldName . ' is should be greater than ' . $expectedValue[0] . '.';
                break;
            case 'maxlen':
                $msg = 'Length of ' . $fieldName . ' is should be lesser than ' . $expectedValue[0] . '.';
                break;
            case 'equal':
                $msg = $fieldName . ' is should be exact ' . $expectedValue[0] . '.';
                break;
            case 'equallen':
                $msg = 'Length of ' . $fieldName . ' is should be exact ' . $expectedValue[0] . '.';
                break;
            case 'between':
                list($from , $to) = explode( ',' , $expectedValue[0] );
                $msg = $fieldName . ' is should in between ' . $from . ' and ' . $to . '.';
                break;
            case 'betweenlen':
                list($from , $to) = explode( ',' , $expectedValue[0] );
                $msg = 'Length of ' . $fieldName . ' is should in between ' . $from . ' and ' . $to . '.';
                break;
            case 'confirmed':
                $msg = $fieldName . ' should match with ' . $fieldName . ' confirmation.';
                break;
            case 'same':
                $msg = $fieldName . ' should match with ' . ucfirst( str_replace( '_' ,
                            ' ' , $expectedValue[0] ) ) . '.';
                break;
            case 'unique':
                $msg = 'Value of ' . $fieldName . ' is exist in the records.';
                break;
            case 'date':
            case 'datetime':
            case 'time':
                $msg = $fieldName . ' is invalid format.';
                break;
            case 'captcha':
                $msg = $fieldName . ' is does not match.';
                break;
            default:
                $msg = $fieldName . ' is invalid.';
                break;
        }

        return $msg;
    }
}