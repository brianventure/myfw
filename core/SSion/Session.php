<?php namespace SSion;

class Session
{
	use \McCade\McCade;

	public function __construct()
	{
		$Session = new SessionFactory();
		$this->load($Session);
	}
}