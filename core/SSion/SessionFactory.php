<?php namespace SSion;

class SessionFactory
{

	public function __construct()
	{
		session_start();
	}

	public function put($key, $value = '')
	{
		if(!is_array($key))
			$_SESSION[$key] = $value;
		else {
			$_SESSION = array_merge($_SESSION, $key);
		}
	}

	public function push($key, $value = '')
	{
		if(strpos($key, '.'))
		{
			$keys = explode('.', $key);
			$a = &$_SESSION;
		    while( count($keys) > 0 ){
		        $k = array_shift($keys);
		        if(!is_array($a)){
		            $a = array();
		        }
		        $a = &$a[$k];
		    }
		    $a = $value;
		}
	}

	public function pull($key)
	{
		if(strpos($key, '.'))
		{
			$temp_session = $_SESSION;
			$keys = explode('.', $key);
			foreach ($keys as $key) {
				if(isset($temp_session[$key]))
				{
					$temp_session = $temp_session[$key];
				}
			}
			return $temp_session;
		}
	}

	public function all()
	{
		$session = '';
		foreach ($_SESSION as $key => $value) {
			if($key != '_flashes_')
				$session[$key] = $value; 
		}
		return $session;
	}

	public function get($key, $default = '')
	{
		if(isset($_SESSION['_flashes_']) && 
			in_array($key,$_SESSION['_flashes_']) && 
			$this->has($key))
		{
			$session = $_SESSION[$key];
			$this->forget($key);
			unset($_SESSION['_flashes_'][$key]);
			return $session;
		}else{
			return $this->has($key)? (empty($_SESSION[$key])? $default:$_SESSION[$key]): $default;
		}
	}

	public function has($key)
	{
		return isset($_SESSION[$key]);
	}

	public function flush()
	{
		$_SESSION = null;
	}

	public function forget($key)
	{
		if($this->has($key))
		{
			$_SESSION[$key] = null;
			unset($_SESSION[$key]);
		}
	}

	public function flash($key,$value)
	{
		$this->put($key,$value);
		if(!isset($_SESSION['_flashes_']))
			$_SESSION['_flashes_'] = [];

		array_push($_SESSION['_flashes_'], $key);
	}
}