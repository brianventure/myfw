<?php namespace Mameil;

abstract class EmailFactory
{

    protected function _verifyCallback( \closure $callback )
    {
        if ( is_null( $callback ) )
        {
            throw new \Exception( "Closure is not implemented properly." );
        }
    }

    protected function _getView( $view , $args = [])
    {
        return $this->_verifyView( $view ) ? View::make( $view ,
                $args )->render() : $view;
    }

    protected function _verifyView( $view )
    {
        $view_file = __DIR__ . '/../../app/views/' . (str_replace('.','/',$view)). '.php';
        return is_file( $view_file );
    }
}