<?php namespace Mameil;

use Mameil\MailService;

class Mameil extends EmailFactory
{
	protected $Service = NULL;

    function __construct() {
        $this->Service = new MailService;
        $this->connect();
    }

    protected function connect()
    {
        $this->Service->isSMTP();
        $this->Service->SMTPAuth = Config::get('mail.SMTPAuth');
        $this->Service->Host = Config::get('mail.host');
        $this->Service->Port = Config::get('mail.port');
        if( $this->Service->SMTPAuth )
        {
            $this->Service->Username = Config::get('mail.username');
            $this->Service->Password = Config::get('mail.password');
            $this->Service->SMTPSecure = Config::get('mail.encryption');
        }
        $this->Service->setFrom(Config::get('mail.from.address'), Config::get('mail.from.name'));
        $this->Service->isHTML(true); 
    }

    public function send( $view , $args = [] , \closure $callback = NULL )
    {
        $this->_verifyCallback( $callback );

        $this->Service->Body = $this->_getView( $view , array_merge(['message'=>$this],$args) );
        call_user_func( $callback , $this );

        return $this->Service->send();
    }

    public function attach($file, array $options = [])
    {
    	$this->Service->addAttachment($file, $options['as']);
    	return $this;
    }

    public function messages($message)
    {
    	$this->Service->Body = $message;
    	return $this;
    }

    public function getMessages()
    {
        return $this->Service->Body;
    }

    public function from($email, $name = '')
    {
    	$this->Service->From = $email;
		$this->Service->FromName = $name;
		return $this;
    }

    public function to($email,$name = '')
    {
    	$this->Service->addAddress($email,$name);
    	return $this;
    }

    public function subject($subject)
    {
        $this->Service->Subject = $subject;
        return $this;
    }

    public function getSubject()
    {
    	return $this->Service->Subject;
    }

    public function cc($email, $name = '')
    {
    	$this->Service->addCC($email, $name = '');
    	return $this;
    }

    public function bcc($email, $name = '')
    {
    	$this->Service->addBCC($email, $name = '');
    	return $this;
    }

    public function replyTo($email, $name = '')
    {
    	$this->Service->addReplyTo($email, $name = '');
    	return $this;
    }

    public function embed($src)
    {
    	$name = time();
    	$this->Service->AddEmbeddedImage($src,$name);
    	return "cid:$name\\";
    }
}