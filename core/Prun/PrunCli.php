<?php namespace Prun;

class PrunCli extends CliFactory implements CliInterface
{
	function __construct() 
	{
		if (PHP_SAPI == "cli") {
			parent::__construct();
			$this->validArgument();
		}
	}

	public function run()
	{
		list($ClassName,$Method) = explode(':', $this->argv[1]);
		call_user_func_array([new $ClassName,$Method], 
			$this->extractArguments());
	}
}