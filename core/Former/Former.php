<?php namespace Former;

class Former extends FormerFactory
{
	public function open(array $options = [])
	{
		$str = "<form{options}>";
		$option[] = '';
		foreach ($options as $key => $value) {
			list($key,$value) = $this->openFormValue($key,$value);
			$option[] = "$key = '$value'";
		}

		return $this->replacing($str,['options'=>implode(' ',$option)]) . $this->hiddenMethod . $this->token();
	}

	protected function token()
	{
		$token = md5(sha1(time()));
		Session::put('_token',$token);
		return $this->hidden('_token',$token);
	}

	public function close()
	{
		return '</form>';
	}

	public function label($key,$value = '', $options = [])
	{
		$asterisk = '';
		$options = ($this->extractToTagProperties($options));
		if(preg_match('/( |^)required( |$)/i', $options))
			$asterisk = '&nbsp;<i style="color:#AA0202;font-size:8px;vertical-align:top;margin-top:5px;" class="fa fa-asterisk"></i>';

		return "<label for='{$key}' ".$options.">{$value}{$asterisk}</label>";
	}

	public function text($key,$value = '', $options = [])
	{
		return "<input type='text' name='{$key}' id='{$key}' value='{$value}' ".($this->extractToTagProperties($options)).">";
	}

	public function date($key,$value = '', $options = [])
	{
		if(!in_array('data-format',array_keys($options)) ){
			$options['data-format'] = 'y-m-d';
		}
		return "<div class='bfh-datepicker' style='background:#FFF;' data-date='{$value}' data-name='{$key}' date-id='{$key}' ".($this->extractToTagProperties($options))."></div>";
	}

	public function email($key,$value = null, $options = [])
	{
		return "<input type='email' name='{$key}' id='{$key}' value='{$value}' ".($this->extractToTagProperties($options)).">";
	}

	public function textarea($key,$value = '', $options = [])
	{
		return "<textarea name='{$key}' id='{$key}' ".($this->extractToTagProperties($options)).">{$value}</textarea>";
	}

	public function hidden($key,$value = '', $options = [])
	{
		return "<input type='hidden' name='{$key}' id='{$key}' value='{$value}' ".($this->extractToTagProperties($options)).">";
	}

	public function password($key, $options = [])
	{
		return "<input type='password' name='{$key}' id='{$key}' ".($this->extractToTagProperties($options)).">";
	}

	public function file($key, $options = [])
	{
		return "<input type='file' name='{$key}' id='{$key}' ".($this->extractToTagProperties($options)).">";
	}

	public function checkbox($key, $value, $checked = false)
	{
		if(!is_bool($checked) && $checked == $value)
			$checked = "checked";
		elseif(is_bool($checked) && $checked)
			$checked = "checked";
		else
			$checked = "";

		return "<input type='checkbox' name='{$key}' id='{$key}' value='{$value}' {$checked}>";
	}

	public function radio($key, $value, $checked = false)
	{
		if(!is_bool($checked) && $checked == $value)
			$checked = "checked";
		elseif(is_bool($checked) && $checked)
			$checked = "checked";
		else
			$checked = "";
		
		return "<input type='radio' name='{$key}' id='{$key}' value='{$value}' {$checked}>";
	}

	public function select($key, array $options = [], $selected = '', array $attrs = [])
	{
		$opts = [];
		$selects = "<select name='{$key}' id='{$key}' ".($this->extractToTagProperties($attrs)).">";
			foreach ($options as $opt => $value) {
				$select = '';
				if(is_array($value))
				{
					$opts[] = "<optgroup label='{$opt}'>";
					foreach($value as $k => $v)
					{
						if($selected == $k)
							$select = 'selected';
						$opts[] = "<option value='{$k}' {$select}>{$v}</option>";
					}
					$opts[] = "</optgroup>";
				} else {
					if($selected == $opt)
						$select = 'selected';
					$opts[] = "<option value='{$opt}' {$select}>{$value}</option>";
				}
			}
		$selects .= implode('',$opts);
		$selects .= "</select>";

		return $selects;
	}

	public function submit($value, $options = [])
	{
		return "<button type='submit' ".($this->extractToTagProperties($options)).">{$value}</button>";
	}

	public function button($value, $options = [])
	{
		return "<button type='button' ".($this->extractToTagProperties($options)).">{$value}</button>";
	}

	public function captcha($key, $value = '' ,$options = [])
	{
		return $this->label($key,"$value <img src='".route('captcha')."'>", $options);
	}

	public function captchaImage()
	{
	    
	    $captcha_question = '';

	    $a = rand(1,9);
	    $o = array_rand(['+'=>'+','-'=>'-']);
	    $b = rand(1,9);

	    $captcha_question = $a.' '.$o.' '.$b.' = ?';
	    $captcha_answer = $o == '+'? $a + $b : $a - $b;

	    Session::put('captcha_answer',$captcha_answer);
	    $width = 120; 
	    $height = 40;  
	    $image = ImageCreate($width, $height);  
	    $black = ImageColorAllocate($image, 100, 100, 100); 
	    $grey = ImageColorAllocate($image, 200, 200, 200); 
	    ImageFill($image, 0, 0, $black); 
	    ImageString($image, 5, $width / ($width / 10), $height / ($height / 10), $captcha_question, $grey); 
	    header("Content-Type: image/jpeg"); 
	    ImageJpeg($image); 
	    ImageDestroy($image); 
	}
}