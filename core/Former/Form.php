<?php namespace Former;

class Form
{
	use \McCade\McCade;

	public function __construct()
	{
		$Form = new Former();
		$this->load($Form);
	}
}