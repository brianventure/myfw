<?php namespace Former;

abstract class FormerFactory
{
	protected $hiddenMethod = '';

	protected function replacing($string, $array)
	{
		foreach($array as $key => $value)
		{
		    $string = str_replace('{'.$key.'}', $value, $string);
		}

		return $string;
	}

	protected function openFormValue($key,$value)
	{
		if(in_array($key,['url','route']))
		{			
			if($key == 'route')
			{
				$params = [];
				if(is_array($value))
				{
					$params = is_array($value[1])? $value[1]: [$value[1]];
					$value = $value[0];
				}

				$value = \FRouter\Route::route($value,$params);
			}
			$key = 'action';
		}
		if($key == 'method')
		{
			if(in_array($value,['put','delete']))
			{
				$this->hiddenMethod = "<input type='hidden' name='_method' value='{$value}'>";
				$value = 'post';
			}
		}
		if($key == 'files' && $value == true)
		{
			$value = "multipart/form-data";
			$key = 'enctype';
		}

		return [$key,$value];
	}

	protected function extractToTagProperties(array $options = [])
	{
		$option = [];
		foreach ($options as $key => $value) {
			if(is_integer($key))
				$option[] = $value;
			else
				$option[] = "$key = '$value'";
		}
		return implode(' ',$option);
	}
}