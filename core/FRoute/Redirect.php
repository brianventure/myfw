<?php namespace FRouter;

class Redirect
{
	use \McCade\McCade;

	public function __construct()
	{
		$Redirect = new RedirectFactory();
		$this->load($Redirect);
	}
}