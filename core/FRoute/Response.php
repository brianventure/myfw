<?php namespace FRouter;

class Response
{
	use \McCade\McCade;

	public function __construct()
	{
		$Response = new ResponseFactory();
		$this->load($Response);
	}
}