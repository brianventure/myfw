<?php namespace FRouter;

class RedirectFactory
{
	protected $route;

	public function redirect()
	{
		header('location:' . $this->getRoute());
	}

	public function back(array $params = [])
	{
		$route = Route::getCurrentRoute(Session::get('_HTTP_REFERER_'));
		return $this->route($route, $params);
	}

	public function getRoute()
	{
		return $this->route;
	}

	public function route($route, array $params = [])
	{
		$this->route = Route::route($route,$params);
		return $this;
	}

	public function to($path = '/')
	{
		$this->route = Route::path($path);
		return $this;
	}

	public function with($key, $value)
	{
		Session::flash($key, $value);
		return $this;
	}

	public function withErrors($validator)
	{
		Session::flash('errors', $validator->errors());
		return $this;
	}

	public function withInputs(array $input = [])
	{
		if(count($input) < 1)
			$input = Input::all();
		Session::put($input);
		return $this;
	}
}