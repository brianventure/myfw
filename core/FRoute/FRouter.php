<?php namespace FRouter;

use closure;

if (PHP_SAPI == "cli") {
    define('HTTP_HOST', 'HTTP_HOST');
    define('REQUEST_URI', 'REQUEST_URI');
    $_SERVER[HTTP_HOST] = 'localhost';
    $_SERVER[REQUEST_URI] = '/';
}

class FRouter extends FRouterFactory
{
    protected $params        = [];
    protected $publicDir      = "public";
    protected $baseFile      = "index.php";
    protected $controllers_prefix = ['get_','post_','put_','delete_'];
    public $Url = '';
    public $HTTP_HOST = '';

    function __construct() {
        $this->publicDir = Config::get('app.doc_root', '');
        $path = trim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH),"/");
        $this->HTTP_HOST = "http://$_SERVER[HTTP_HOST]/";
        if(preg_match('/^('.$this->publicDir.'\/*)/i', $path)) 
            $this->Url = $this->HTTP_HOST.(str_replace('\/','/',$this->publicDir));
        else
            $this->Url = rtrim('/',$this->HTTP_HOST);
    }

    public function routesName()
    {
        return array_keys($this->RoutesAs);
    }

    public function getCurrentPath()
    {
        return $this->currentPath;
    }

    public function getCurrentRoute($path = '')
    {
        if($path != '')
        {
            return array_flip($this->RoutesAs)[$path];
        }else{
            return array_flip($this->RoutesAs)[rtrim($this->Url, '/')."/{$this->baseFile}/".$this->currentPath];
        }
    }

    public function controller($name,$controller)
    {
        $class = new ReflectionClass($controller);
        $methods = $class->getMethods();
        foreach ($methods as $method) {
            list($head) = explode('_', $method->name);
            if(in_array($head.'_',$this->controllers_prefix))
            {
                $lastname = str_replace($head.'_', '', $method->name);
                $this->$head($name.'/'.$lastname,[
                    'as'=> $name.'.'.$lastname,
                    'uses'=>$controller.'@'.$method->name,
                    ]);
            }
        }
    }

    public function get($name,$controller)
    {
        if(!is_array($controller))
            $this->assignRoute($name,$controller,'Get');
        else
        {
            if(isset($controller['uses']))
                $this->assignRoute($name,$controller['uses'],'Get');
            else
                $this->assignRoute($name,$controller['do'],'Get');

            $this->assignAs($name, $controller);
            if(isset($controller['before']))
                $this->assignFilter($name, $controller['before']);
        }            
    }

    public function post($name,$controller)
    {
        if(!is_array($controller))
            $this->assignRoute($name,$controller,'Post');
        else
        {
            if(isset($controller['uses']))
                $this->assignRoute($name,$controller['uses'],'Post');
            else
                $this->assignRoute($name,$controller['do'],'Post');
            
            $this->assignAs($name, $controller);
            if(isset($controller['before']))
                $this->assignFilter($name, $controller['before']);
        } 
    }

    public function put($name,$controller)
    {
        if(!is_array($controller))
            $this->assignRoute($name,$controller,'Put');
        else
        {
            if(isset($controller['uses']))
                $this->assignRoute($name,$controller['uses'],'Put');
            else
                $this->assignRoute($name,$controller['do'],'Put');

            $this->assignAs($name, $controller);
            if(isset($controller['before']))
                $this->assignFilter($name, $controller['before']);
        }
    }

    public function delete($name,$controller)
    {
        if(!is_array($controller))
            $this->assignRoute($name,$controller,'Delete');
        else
        {
            if(isset($controller['uses']))
                $this->assignRoute($name,$controller['uses'],'Delete');
            else
                $this->assignRoute($name,$controller['do'],'Delete');

            $this->assignAs($name, $controller);
            if(isset($controller['before']))
                $this->assignFilter($name, $controller['before']);
        }
    }
    

    public function filter($key, closure $closure)
    {
        App::bind($key,$closure);
    }

    public function route($name,array $params = [])
    {
        $route = $this->RoutesAs[$name];
        if(count($params) > 0 )
        {
            $i = 0;
            $route = preg_replace_callback("/\{([a-z0-9]+?)\}/i", function ($result)
            use ($params, &$i) {
                $i++;
                return $params[$i-1];
            }, $route);

        }

        return $route;
    }

    public function routes()
    {
        return $this->RoutesAs;
    }
     
    public function run() {
        $return = null;
        $this->parseUri();
        if($this->filter != '')
        {
            if(preg_match('/^\w+\|\w+$/', $this->filter))
            {   
                $filters = explode('|',$this->filter);
                foreach ($filters as $filter) {
                    $app = App::make($filter);
                    if($app instanceof RedirectFactory)
                        return $app->redirect();
                    else
                        echo $app;
                }
            }
            else
            {
                $app = App::make($this->filter);
                if($app instanceof RedirectFactory)
                    return $app->redirect();
                else
                    echo $app;               
            }
        }

        if(empty($this->controller) && !empty($this->callback))
        {
            $return = call_user_func_array($this->callback, $this->params);
        }

        if(!empty($this->controller) && 
            !is_null($this->controller))
        {
            if(App::has($this->controller))
            {
                $return = call_user_func_array(array(App::make($this->controller), $this->callback), $this->params);
            }
            else
            {
                $return =  call_user_func_array(array(new $this->controller, $this->callback), $this->params);
            }
        }

        if(is_null($return))
        {
            echo View::make('errors.404')->render();
        } else {
            if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']))
                Session::put('_HTTP_REFERER_',$this->HTTP_HOST . trim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH),"/"));
            if($return instanceof RedirectFactory)
                return $return->redirect();
            else
                echo $return;
        }
    }
}