<?php namespace FRouter;

abstract class FRouterFactory
{
    protected $filters          = [];
    protected $RoutesAs         = [];
	protected $GetRoutes    	= [];
	protected $PostRoutes    	= [];
	protected $PutRoutes    	= [];
	protected $DeleteRoutes    	= [];

    protected function assignRoute($name,$controller,$method = 'Get')
    {
        $method = "{$method}Routes";
        $this->{$method}[$name] = $controller;
    } 

    protected function assignAs($name,$controller)
    {
        if($name == '/')
        {
            $name = '';
        }    
        if(isset($controller['as']))
        {   
            $path = trim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH),"/");
            if(preg_match('/^('.$this->publicDir.'\/*)/i', $path)) 
            {
                if($this->publicDir == '')
                    $string = "$this->baseFile/$name";
                else
                    $string = "/$this->baseFile/$name";
                $this->RoutesAs[$controller['as']] = $this->HTTP_HOST.(str_replace('\/','/',$this->publicDir)).$string;
            }
            else
                $this->RoutesAs[$controller['as']] = $this->HTTP_HOST."$name";
        }
    }

    protected function assignFilter($name, $key)
    {
        $this->filters[$name] = $key;
    }

	protected function parseUri()
    {
        $path = trim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH),"/"); 
        $path = str_replace('//', '/', $path);
        $path = preg_replace('/^('.$this->publicDir.'\/*)/i', "", $path);
        $path = preg_replace('/^('.$this->baseFile.'\/*)/i', "", $path);
        if(empty($path))
        	$path = '/';
        
        if($_SERVER['REQUEST_METHOD'] === 'GET')
        {
            $this->extractController($path,'Get');
        }

        if($_SERVER['REQUEST_METHOD'] === 'POST')
        {
            if(isset($_POST['_method']) and !empty($_POST['_method']))
            {
                $this->extractController($path,ucfirst(strtolower($_POST['_method'])));
            } else {
                $this->extractController($path,'Post');
            }
        }

        if($_SERVER['REQUEST_METHOD'] === 'PUT')
        {
            $this->extractController($path,'Put');
        }

        if($_SERVER['REQUEST_METHOD'] === 'DELETE')
        {
            $this->extractController($path,'Delete');
        }
    }

    protected function extractController($path,$method)
    {
    	$method = "{$method}Routes";

        $this->currentPath = $path = $this->extractPath($path,$method);

        $this->filter = isset($this->filters[$path])? $this->filters[$path]: '';

        if(isset($this->{$method}[$path]) && is_callable($this->{$method}[$path]))
        {
            $this->controller = '';
            $this->callback = $this->{$method}[$path];
            return;
        }

        if(in_array($path, array_keys($this->{$method})))
        {
            list($controller,$callback) = explode('@', $this->{$method}[$path]);
            $this->controller = $controller;
            $this->callback = $callback;
            return;
        }

    }

    protected function extractPath($path,$method)
    {
        $s = preg_grep("/(\/{.+\?*})+/i",array_keys($this->{$method}));
        $break_path = explode('/', $path);
        foreach ($s as $possible_path) {
            $break_possible_path = explode('/', trim($possible_path,'/'));
            if(count($break_possible_path) == count($break_path))
            {
                $count_path = count($break_path);
                $check = 0;
                $params = [];
                foreach (explode('/', trim($possible_path,'/')) 
                    as $key => $value) {
                    if($value == $break_path[$key] ||
                        preg_match('/{.+\?*}/i', $value))
                    {
                        if(preg_match('/{.+\?*}/i', $value))
                        {
                            $params[] = isset($break_path[$key])? $break_path[$key]:'';
                        }
                        $check++;
                    }
                }

                if($check == $count_path)
                {
                    $this->params = $params;
                    return $path = $possible_path;
                }
            }
        }

        return $path;
    }
}