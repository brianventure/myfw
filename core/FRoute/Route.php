<?php namespace FRouter;

class Route
{
	use \McCade\McCade;

	public function __construct()
	{
		$Router = new FRouter();
		$this->load($Router);
	}
}