<?php

class Library extends \GlObserver\Event
{

	function updateRecordBooks()
	{
		$this->__notify();
	}

	function newBookEntry()
	{
		$this->__notify();
	}
}