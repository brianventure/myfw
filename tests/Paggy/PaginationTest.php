<?php namespace Paggy;
error_reporting(-1);
ini_set('display_errors', 1);

require_once __dir__.'/../../app/bootstrap.php';

/**
 * PaginationTest
 *
 * @group group
 */
class PaginationTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @covers class::make()
     */
    public function testMake()
    {
    	$test = DB::table('tests')->paginate(2);
		foreach ($test as $t) {
			var_dump($t->name .' '. $t->state);
		}
    }
}
