<?php
require_once __dir__.'/../../app/bootstrap.php';
error_reporting(E_ALL);
ini_set('display_errors', -1);

/**
 * ModelTest
 *
 * @group group
 */
class ModelTest extends \PHPUnit_Framework_TestCase
{

    public function testCallModel()
    {
    	$T = Test::find('alice');
    	// var_dump($T);
    }
     
    public function testCreate()
    {
    	$T = new Test;
		$T->name = 'Wiki';
		$T->state = 888;
		$T->save();
		// var_dump($T);
    }

    public function testUpdate()
    {
    	$T = Test::find('Wiki');
    	$T->name = 'Wiki';
		$T->state = 999;
		$T->save();
		// var_dump($T);
    }

    public function testFindAll()
    {
    	$T = Test::all();
    	var_dump($T);
    }

    public function testWhere()
    {
    	$t = Test::where('name','like','%j%')->where('state','=',0)->get();
    	// var_dump($t);
    }

    public function testStaticScope()
    {
    	$t = Test::AllJ()->orWhere('name','=','wiki')->get();
    	// var_dump($t);
    }

    /*public function testNonStaticScope()
    {
    	$this->markTestIncomplete();
    	$t = Test::AllJ()->State(999)->get();
    	var_dump($t);
    }*/

    public function testRelation()
    {
        $T = Test::with('Test1')->get();
        foreach ($T as $test) {
            foreach ($test->Test1 as $Test1) {
                // var_dump($Test1);
            }
        }        
        $T = Test1::with('Test')->get();
        foreach ($T as $test1) {
            foreach ($test1->Test as $Test) {
                // var_dump($Test);
            }
        }
    }

    public function testWhereDelete()
    {
    	Test::where('name','=','wiki')->delete();
    	$t = Test::where('name','=','wiki')->get();
    }

    public function testDelete()
    {
    	Test::delete();
    }
}
