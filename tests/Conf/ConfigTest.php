<?php namespace Conf;

require_once __dir__.'/../../app/bootstrap.php';

/**
 * ConfigTest
 *
 * @group group
 */
class ConfigTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @covers class::ableToSetAndGetData()
     */
    public function testAbleToSetAndGetData()
    {
    	$example1 = [
    		'example1'=>[
    			'test'=>'test1',
    			'test2'=>'test3',
    			'test4'=>[
    				'test5'=>'test6',
    				'test7'=>'test8'
    			]
    		]
    	];

    	Config::set($example1);
    	$result = Config::get('example1.test4.test7');

    	$this->assertEquals('test8', $result);
    }

    /**
     * @covers class::ableToGetDataFromConfigDirectory()
     */
    public function testAbleToGetDataFromConfigDirectory()
    {
    	$result = Config::get('example.test');
    	$this->assertEquals('test1', $result);
    	$result1 = Config::get('example.test2');
    	$this->assertCount(2, $result1);
    }
}
