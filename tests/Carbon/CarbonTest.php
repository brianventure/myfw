<?php namespace Carbon;
error_reporting(-1);
ini_set('display_errors', 1);

require_once __dir__.'/../../app/bootstrap.php';

class CarbonTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @covers class::Usage()
     */
    public function testUsage()
    {
		printf("Right now is %s", Carbon::now()->toDateTimeString());
		printf("Right now in Vancouver is %s", Carbon::now('America/Vancouver'));  //implicit __toString()
		printf("Right now in Kuala Lumpur is %s", Carbon::now('Asia/Kuala_Lumpur'));  //implicit __toString()
		$tomorrow = Carbon::now()->addDay();
		$lastWeek = Carbon::now()->subWeek();
		$nextSummerOlympics = Carbon::createFromDate(2012)->addYears(4);
		$officialDate = Carbon::now()->toRFC2822String();

		$howOldAmI = Carbon::createFromDate(1975, 5, 21)->age;

		$noonTodayLondonTime = Carbon::createFromTime(12, 0, 0, 'Europe/London');

		$worldWillEnd = Carbon::createFromDate(2012, 12, 21, 'GMT');

		// Don't really want to die so mock now
		Carbon::setTestNow(Carbon::createFromDate(2000, 1, 1));

		// comparisons are always done in UTC
		if (Carbon::now()->gte($worldWillEnd)) {
		   die();
		}

		// Phew! Return to normal behaviour
		Carbon::setTestNow();

		if (Carbon::now()->isWeekend()) {
		   echo 'Party!';
		}
		echo Carbon::now()->subMinutes(2)->diffForHumans(); // '2 minutes ago'

		// ... but also does 'from now', 'after' and 'before'
		// rolling up to seconds, minutes, hours, days, months, years

		$daysSinceEpoch = Carbon::createFromTimeStamp(0)->diffInDays();
    }
}

