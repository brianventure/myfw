<?php namespace McCade;

class Calculator
{
	use \McCade\McCade;

	public function __construct($DI = NULL)
	{
		$this->load(new \Libs\CalculatorBuilder($DI));
	}

	protected function functionInCalculator()
	{
		return 'Can I\'m called?';
	}
}


new Calculator(new \Libs\Building);