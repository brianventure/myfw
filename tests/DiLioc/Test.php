<?php

/**
 * undocumented class
 *
 * @package default
 * @author 
 **/
class Test
{
	public $var, $var2 = '';
	function __construct($var,$var2='') {
		$this->var = $var;
		$this->var2 = $var2;
	}

	public function out()
	{
		$var2 = '';
		if($this->var2 !='')
		{
			$var2= ' and '.$this->var2->out();	
		}

		return "I'm " . $this->var . $var2;
	}

	public function sum($a,$b,$c)
	{
		$this->sum = $a + $b + $c;
	}

	public function getSum()
	{
		return $this->sum;
	}

	function __toString()
	{
		return $this->var;
	}
}