<?php namespace DiLioc;

require_once __dir__.'/../../app/bootstrap.php';

require_once __dir__ . '/Test.php';
require_once __dir__ . '/TestClass2.php';
require_once __dir__ . '/otherClass.php';
require_once __dir__ . '/DependencyInjection.php';

/**
 * DiLiocTest
 *
 * @group group
 */
class DiLiocTest extends \PHPUnit_Framework_TestCase
{

	public function setUp()
	{
		$this->DiLioc = new App;
	}

	public function testClosureBindingAndMake()
	{
		$this->DiLioc->bind('Test1',function($a,$b,$c){
    		$Test1 = new \Test('Wiki');
    		$Test1->sum($a,$b,$c);
    		return $Test1;
    	},1,2,3);
    	$this->assertEquals("I'm Wiki", $this->DiLioc->make('Test1')->out());
    	$this->assertEquals(6,$this->DiLioc->make('Test1')->getSum());

    	$DiLioc = $this->DiLioc;
    	$this->DiLioc->bind('Test2',function() use($DiLioc){
    		$DiLioc->make('Test1')->getSum();
    		return new \Test('Pai');
    	});
    	$this->assertEquals("I'm Pai", $this->DiLioc->make('Test2')->out());
	}

	/**
	 * @covers class::DependencyInjection()
	 */
	public function testDependencyInjection()
	{
		$this->DiLioc->bind('Test3','Test',new \DependencyInjection);
    	$this->assertEquals("I'm Dependency Injection", $this->DiLioc->make('Test3')->out());
	}

	public function testMoreDependencyInjection()
	{
		$this->testClosureBindingAndMake();

		$this->DiLioc->bind('Test4','Test',new \DependencyInjection, $this->DiLioc->make('Test1'));
    	$this->assertEquals("I'm Dependency Injection and I'm Wiki", $this->DiLioc->make('Test4')->out());

    	$this->DiLioc->bind('Tests4','TestClass2',new \DependencyInjection, $this->DiLioc->make('Test1'), $this->DiLioc->make('Test2'));
    	$this->assertEquals("I'm Dependency Injection and Wiki and Pai", $this->DiLioc->make('Tests4')->out());

	}

    public function testAutoDependencyInjection()
    {
    	$this->DiLioc->bind('Test5','TestClass3');
    	$this->assertEquals("from TestClass3 calling TestClass I am from TestClass", $this->DiLioc->make('Test5')->out());

    	$this->DiLioc->bind('Test6','TestClass5');
    	$this->assertEquals("I am from TestClass5", $this->DiLioc->make('Test6')->out());

    	$this->DiLioc->bind('TestClass2','TestClass2','A','B','C'); //binded for purpose auto dependency injection in TestClass4 2nd arguement
    	$this->DiLioc->bind('Test7','TestClass4');
    	$this->assertEquals("I am from TestClass and from TestClass2 I'm A and B and C", $this->DiLioc->make('Test7')->out());
    	
    }

    /**
     * @covers class::BindKeySameNameAsClassName()
     */
    public function testBindKeySameNameAsClassName()
    {
    	$this->DiLioc->bind('TestClass2','TestClass2','A','B','C');
    	$this->assertEquals("I'm A and B and C", $this->DiLioc->make('TestClass2')->out());
    }

    public function testBindKeyWithMakeClassByIOC()
    {
    	$this->DiLioc->bind('TestClass2','TestClass2','1','2','3');
    	$this->DiLioc->bind('TestTest',$this->DiLioc->make('TestClass2'));
    	$this->assertEquals("I'm 1 and 2 and 3", $this->DiLioc->make('TestTest')->out());
    }

    public function testBindKeyWithBindedKey()
    {
    	$this->markTestIncomplete('Not yet implemented');
    	$this->DiLioc->bind('TestClass2','TestClass2','1','2','3');
    	$this->DiLioc->bind('TestTest','TestClass2');
    	$this->assertEquals("I'm 1 and 2 and 3", $this->DiLioc->make('TestTest')->out());
    }
}

