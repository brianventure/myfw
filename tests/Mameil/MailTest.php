<?php namespace Mameil;
error_reporting(-1);
ini_set('display_errors', 1);

require_once __dir__.'/../../app/bootstrap.php';
/**
 * MailTest
 *
 * @group group
 */
class MailTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @covers class::Send()
     */
    /*public function testSend()
    {
    	$result = Mail::send('welcome', ['contents'=>'Hello World'], function($message)
		{
			$message->from('realcoder@home.com','Real Coder');
			// $message->cc('Efendi@seda.gov.my','Efendi');
		    $message->to('wikichua@gmail.com', 'Wiki Chua')->subject('Welcome!');
		    $message->attach(__dir__.'/../../public/img/kiss-my-ass1.jpg', array('as' => 'kiss.jpg', 'mime' => 'jpeg'));
		});

		$this->assertTrue($result, 'Email fail?');
    }*/

    /**
     * @covers class::SEDASmtp()
     */
    public function testSEDASmtp()
    {
		Mail::send('emails.test', [], function($message)
		{
		    $message->to('wikichua@gmail.com', 'Wiki Chua')->subject('Welcome!');
		});    	
    }
}
