<?php namespace Vladitor;
error_reporting(-1);
ini_set('display_errors', 1);

require_once __dir__.'/../../app/bootstrap.php';

class ValidatorTest extends \PHPUnit_Framework_TestCase
{
    protected $_post = [];
	protected $_rules = [];
	protected $_messages = [];

	protected $_errors = [];

    public function setUp()
    {
    	$this->_post = [
    		'name' => 'wiki',
    		'email' => 'wikichua@gmail.com',
    		'dob' => '1983-01-07',
    		'number' => '10',
    		'between' => '5' ,
        ];

    	$this->_rules = [
    		'name' => 'required',
    		'email' => 'required|email',
    		'dob' => 'date' ,
            'number' => 'required|min:15',
    		'between' => 'between:4,6' ,
            'regexp' => 'regexp:^[php]+$' ,
            'password' => 'confirmed' ,
            'samefield1' => 'required|same:samefield2',
            'maxlen' => 'maxlen:5',
            'betweenlen' => 'betweenlen:2,5',
            'unique' => 'unique:users.loginid', //table.field
        ];

    	$this->_messages = [
    		'name' => [
    			'required'=>'Who are you?',
    		], 
    		'number' => [
    			'required'=>'give me a number.',
    			'min'=>'at least 15',
    		],
    		'regexp'=>[
    			'regexp' => 'where is php?',
    		]
    	];
    }

    public function tearDown()
    {
    	unset($this->_post);
    }

    public function testPostSetUp()
    {
    	$this->assertGreaterThan(0, $this->_post);
    }

    public function testMisc()
    {
    	$each_rule = preg_match('/\|+/','required')? explode('|','required'):['required'];
    	$this->assertCount(1, $each_rule);

		unset($each_rule);
		$each_rule = preg_match('/\|+/','required|another')? explode('|','required|another'):['required'];
    	$this->assertCount(2, $each_rule);
    }

    public function testGetCorrectMessageSetByUser()
    {
        
    	// $message = Validator::getDefaultMessage('required','name',$this->_messages,$expectedValues = []);
    	// $this->assertEquals('Who are you?', $message);

    	// $message = Validator::getDefaultMessage('required','email',$this->_messages,$expectedValues = []);
    	// $this->assertEquals('Email is required.', $message);

    }

    public function testCallSuccessValidator()
    {
    	$post = [
    		'name' => 'wiki',
    		'email' => 'wikichua@gmail.com',
    		'dob' => '1983-01-07',
    		'number' => '16',
    		'between' => 5 ,
            'password' => 'asdasd' ,
            'password_confirmation' => 'asdasd' ,
            'samefield1' => 'same value' ,
            'samefield2' => 'same value' ,
            'maxlen' => 'abcde',
            'betweenlen' => 'abcd',
        ];

       	$valid = Validator::make($post,$this->_rules,$this->_messages);
       	$this->assertCount(0, $valid->errors()); 
    }

    public function testCallFailValidator()
    {
    	$post = [
    		'name'=>'',
    		'email'=>'',
    		'dob' => '1983/01/32' ,
            'number' => 14,
    		'between' => 5,
    		'regexp' => 'sdf' ,
            'password' => 'asdasd' ,
            'password_confirmation' => 'assdfsdfdasd' ,
            'samefield1' => 'same value' ,
            'samefield2' => 'same not value' ,
            'maxlen' => 'abcdef',
            'betweenlen' => 'a',
            // 'unique' => 'SS11223344', // can't due to stupid function 
        ];
    	$valid = Validator::make($post,$this->_rules,$this->_messages);
       	$this->assertTrue($valid->fails());
       	$this->assertTrue(is_array($valid->errors()));
       	var_dump($valid->errors());  	
    }
}
