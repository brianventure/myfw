<?php namespace Former;
require_once __dir__.'/../../app/bootstrap.php';
/**
 * FormTest
 *
 * @group group
 */
class FormTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @covers class::OpenFormAndClose()
     */
    public function testOpenFormAndClose()
    {
    	$form = new Former;

    	$this->assertSame("<form name = 'form1' action = 'http://localhost/check/testing' class = 'theclass' method = 'post'>", $form->open([
    		'name'=>'form1',
    		'route'=>['check','testing'],
    		'class'=>'theclass',
    		'method'=>'post'
    		]));
    	$this->assertSame("<form name = 'form1' action = 'http://localhost/check/testing' class = 'theclass' method = 'post'><input type='hidden' name='_method' value='put'>", $form->open([
    		'name'=>'form1',
    		'route'=>['check','testing'],
    		'class'=>'theclass',
    		'method'=>'put'
    		]));
    	$this->assertSame("<form name = 'form1' action = 'http://localhost/check/testing' class = 'theclass' method = 'post'><input type='hidden' name='_method' value='delete'>", $form->open([
    		'name'=>'form1',
    		'route'=>['check','testing'],
    		'class'=>'theclass',
    		'method'=>'delete'
    		]));

    	$this->assertSame("<label for='email' class = 'awesome'>Email</lable>",$form->label('email','Email',['class'=>'awesome']));
    	$this->assertSame("<input type='text' name='email id='email' value='example@gmail.com' >", $form->text('email', 'example@gmail.com'));

    	echo $form->select('animal', array(
		    'Cats' => array('leopard' => 'Leopard', 'tiger' => 'Tiger'),
		    'Dogs' => array('spaniel' => 'Spaniel'),
		));

		echo $form->select('size', array('L' => 'Large', 'S' => 'Small'), 'S');

		echo $form->submit('Login');

    	$this->assertSame('</form>', $form->close());
    }
}
